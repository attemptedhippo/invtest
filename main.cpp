#include <iostream>
#include "item.h"
#include "bag.h"
#include "itemDefines.h"

using namespace std;

int main() {
	int gold = 500;
	bag storeInv;
	bag inventory;
	storeInv.add(unique_ptr<item>(ITEM_RPOTION(5)));
	storeInv.add(unique_ptr<item>(ITEM_ISWORD(5)));
	storeInv.add(unique_ptr<item>(ITEM_ICHEST(5)));
	storeInv.add(unique_ptr<item>(ITEM_ILEGS(5)));

	inventory.add(unique_ptr<item>(ITEM_RPOTION(1)));
	inventory.add(unique_ptr<item>(ITEM_ISWORD(1)));
	inventory.add(unique_ptr<item>(ITEM_ILEGS(1)));
	int storeListNum = 1;
	cout << "In store:\n";
	for (auto &i : storeInv) {
		cout << storeListNum << ". ";
		cout << i->getQuant() << "x" << i->getName() << " : " << i->getPrice() << "g" << endl;
		storeListNum++;
	}
	
	int playerListNum = 1;
	cout << "\nIn inventory: " << gold << "g\n";
	for (auto &i : inventory) {
		cout << playerListNum << ". ";
		cout << i->getQuant() << "x" << i->getName() << " : " << i->getPrice() << "g" << endl;
		playerListNum++;
	}

	cin.get();

	return 0;
}