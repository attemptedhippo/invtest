#pragma once
#include <list>
#include <utility>
#include <memory>
#include "item.h"

class bag {
private:
	std::list<std::unique_ptr<item>> inventory;

public:
	const unsigned int maxItem;

	bag() : maxItem(10) { }
	bag(int _maxItem) : maxItem(_maxItem) { }

	bool add(std::unique_ptr<item> _item) {
		if (maxItem > 0 && inventory.size() >= maxItem) {
			//Inventory full
			std::cout << "couldnt add item\n";
			return false;
		}else {
			for (std::list<std::unique_ptr<item>>::iterator it = inventory.begin(); it != inventory.end(); ++it) {
				if (it->get()->getNum() == _item->getNum()) {
					(*it)->addToStack(_item->getQuant());
					return true;
				}
			}
			inventory.push_back(move(_item));
			return true;
		}
	}
	
	void remove(std::unique_ptr<item> _item) {
		item *tItem = _item.get();
		for (std::list<std::unique_ptr<item>>::iterator it = inventory.begin(); it != inventory.end(); ++it) {
			if (it->get()->getNum() == tItem->getNum()) {
				(*it)->addToStack(-tItem->getQuant());
			}
		}

	}

	item* get(int _itemID) {
		for (std::list<std::unique_ptr<item>>::iterator it = inventory.begin(); it != inventory.end(); ++it) {
			if (it->get()->getNum() == _itemID) {
				return it->get();
			}
		}
		return nullptr;
	}

	std::list<std::unique_ptr<item>>::const_iterator begin() {
		return inventory.begin();
	}
	std::list<std::unique_ptr<item>>::const_iterator end() {
		return inventory.end();
	}

	unsigned int size() {
		return inventory.size();
	}
};
